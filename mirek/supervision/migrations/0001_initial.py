# Generated by Django 3.0 on 2020-09-10 17:37

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Control',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('mineshaft', models.CharField(choices=[('leon4', 'Leon IV'), ('leon2', 'Leon II')], default='Leon IV', max_length=12)),
                ('area', models.CharField(choices=[('1200', '1200'), ('1150', '1150'), ('1067', '1067'), ('1000', '1000'), ('zrab', 'zrąb'), ('800', '800'), ('600', '600'), ('nadszybie', 'nadszybie')], default='800', max_length=12)),
                ('installation', models.CharField(max_length=200)),
                ('cycle', models.CharField(choices=[(42, 'sześcio-tygodniowa'), (730, 'co dwa lata'), (180, 'pół-roczna'), (7, 'tygodniówka'), (365, 'roczna'), (90, 'kwartalna'), (1825, 'co pięć lat'), (30, 'miesiączka')], default='tygodniówka', max_length=30)),
                ('last_control_date', models.DateField(default=django.utils.timezone.now)),
                ('performing', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
