from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from datetime import date, timedelta
# Create your models here.




class Control(models.Model):
	SHAFTS = {
		('leon4', 'Leon IV'),
		('leon2', 'Leon II')
	}

	AREAS = {
		('nadszybie', 'nadszybie'),
		('zrab', 'zrąb'),
		('600', '600'),
		('800', '800'),
		('1000', '1000'),
		('1067', '1067'),
		('1150', '1150'),
		('1200', '1200'),
	}

	CYCLE = {
		('7', 'tygodniówka'),
		('30', 'miesiączka'),
		('42', 'sześcio-tygodniowa'),
		('90', 'kwartalna'),
		('180', 'pół-roczna'),
		('365', 'roczna'),
		('730', 'co dwa lata'),
		('1825', 'co pięć lat'),

	}

	mineshaft = models.CharField(max_length=12, choices=SHAFTS, default='leon2')
	area = models.CharField(max_length=12, choices=AREAS, default='800')
	installation = models.CharField(max_length=200)
	comments = models.CharField(max_length=255, blank=True)
	cycle = models.CharField(max_length=30, choices=CYCLE, default='7')

	performing = models.ForeignKey(User, on_delete=models.DO_NOTHING)
	next_control_date = models.DateField(default=timezone.now)

	@property
	def last_control_date(self):
		"""date of last control"""
		td = timedelta(days=int(self.cycle))
		return self.next_control_date - td

	@property
	def late_control(self):
		td = self.next_control_date - date.today()
		# print(td.days)
		if td.days <= 0:
			return False
		else:
			return True


	def __str__(self):
		return f'{self.mineshaft},{self.area},{self.installation} ({self.cycle})'
