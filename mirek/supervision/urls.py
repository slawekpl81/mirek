from django.contrib import admin
from django.urls import path, include
from .views import *

app_name = 'supervision'

urlpatterns = [
    path('all_controls/', All_controls.as_view(), name='allcontrols'),
    path('controls/<option>/', Filter_controls.as_view(), name='filtercontrols'),
    path('new_control', NewControl.as_view(), name='newcontrol'),
    path('update_control/<int:pk>', UpdateControlView.as_view(), name='updatecontrol'),
    path('delete_control/<int:pk>', DeleteControlView.as_view(), name='deletecontrol'),
    path('make_control/<int:control_id>', make_control_view, name='makecontrol'),
]
