from django.shortcuts import render, redirect
from django.views.generic import ListView, TemplateView, CreateView, DetailView,\
    UpdateView, DeleteView
from django.urls import reverse_lazy
from django.shortcuts import get_object_or_404
from django.contrib.auth.mixins import LoginRequiredMixin,PermissionRequiredMixin

from .models import Control
from .forms import *
from datetime import date, timedelta
# Create your views here.

# def all_controls(request):
#     all_controls = Control.objects.all()
#     context = {'controls': all_controls}
#     return render(request, 'controls.html', context)

class All_controls(LoginRequiredMixin, ListView):
    template_name = 'controls.html'
    model = Control
    context_object_name = 'controls'
    #queryset = Control.objects.all()
    #paginate_by = 10
    ordering = ['next_control_date']

class Filter_controls(LoginRequiredMixin, ListView):
    template_name = 'controls.html'
    context_object_name = 'controls'
    ordering = ['next_control_date']
    #paginate_by = 10

    def get_queryset(self):
        option = self.kwargs['option']
        if option == 'late':
            return Control.objects.filter(next_control_date__lte=date.today())
        elif option == 'incoming':
            td = timedelta(days=15) + date.today()
            return Control.objects.filter(next_control_date__gt=date.today()).\
            filter(next_control_date__lte=td)
        else:
            return Control.objects.filter(mineshaft=option)


class NewControl(LoginRequiredMixin, CreateView):
    model = Control
    #form_class = ControlForm
    #fields = ['mineshaft', 'area', 'installation', 'comments', 'cycle', 'next_control_date']
    fields = '__all__'
    template_name = 'control_form.html'
    success_url = reverse_lazy('supervision:allcontrols')

    def get_initial(self):
        performing = self.request.user
        return {'performing': performing}

class UpdateControlView(LoginRequiredMixin, UpdateView):
    model = Control
    fields = '__all__'
    template_name = 'control_form.html'
    success_url = reverse_lazy('supervision:allcontrols')
    context_object_name = 'control'

def make_control_view(request, control_id):
    form = MakeControlForm(initial={'date':date.today()})
    if request.method == 'POST':
        form = MakeControlForm(request.POST)
        if form.is_valid():
            next_date = form.cleaned_data['date']
            control = get_object_or_404(Control, pk=control_id)
            control.next_control_date = next_date
            control.save()
        return redirect('supervision:allcontrols')
    return render(request, 'control_make.html', {'form': form})


class DeleteControlView(LoginRequiredMixin, DeleteView):
    model = Control
    template_name = 'control_delete.html'
    success_url = reverse_lazy('supervision:allcontrols')
